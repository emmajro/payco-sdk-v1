/*
 * Copyright (c) 2021. Soluciones Informáticas Manzanares - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited,
 * proprietary and confidential.
 * Written by Pedro Parra <pedrop@manzanares.com.ve>.
 * Last Modified 26/8/21 4:23 p. m.
 */

package com.payco.lukasdk

import android.content.Context
import androidx.activity.result.ActivityResultCaller
import androidx.activity.result.ActivityResultLauncher
import androidx.annotation.CheckResult
import androidx.annotation.MainThread
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.google.gson.Gson
import com.payco.lukasdk.bridge.LukaBridgeResponse
import com.payco.lukasdk.bridge.LukaGateway
import com.payco.lukasdk.exceptions.LukaHttpException
import com.payco.lukasdk.http.ApiErrorResponse
import com.payco.lukasdk.http.ApiRequest
import com.payco.lukasdk.http.ApiResponse
import com.payco.lukasdk.http.LukaServiceProvider
import com.payco.lukasdk.lifecycle.*
import com.payco.lukasdk.services.auth.LukaAuthCredentials
import com.payco.lukasdk.services.auth.LukaSession
import com.payco.lukasdk.services.luka.LukaConfig
import com.payco.lukasdk.services.luka.bluesnap.BluesnapAuth
import com.payco.lukasdk.services.luka.transaction.AuthorizedTransaction
import com.payco.lukasdk.services.luka.transaction.LukaTransaction
import com.payco.lukasdk.transaction.*
import retrofit2.Retrofit
import java.util.*
import java.util.concurrent.TimeUnit

open class LukaApi(
    internal val mContext: Context,
    internal val mGson: Gson,
    lukaRetrofit: Retrofit,
    testModeEnabled: Boolean,
) {
    private val mLukaServiceProvider = LukaServiceProvider(lukaRetrofit)
    private val mCredentials = LukaAuthCredentials()
    private val mAuthRequest: ApiRequest<Map<String, Any>>
    internal val mSession: LiveData<LukaSession?>
    internal val mTestMode: Boolean = testModeEnabled
    internal lateinit var mLukaResponse : MutableLiveData<LukaBridgeResponse>
    private val mConfig: LiveData<out Holder2<LukaConfig, LukaSession>?>
    internal lateinit var mCallbacks: Map<LukaGateway, ActivityResultLauncher<LukaPaymentRequest>>

    init {
        mAuthRequest = mLukaServiceProvider.auth()
            .login(mCredentials)
            .withAutoLaunch(10, TimeUnit.MINUTES)
            .withAutoRefresh(5, TimeUnit.MINUTES)
            .withAutoRetry(6, TimeUnit.SECONDS)

        mSession = mAuthRequest.response.map { response ->
            response.headers()?.let {
                val id = it["id"]
                val token = it["token"]

                if (id != null && token != null) {
                    return@let LukaSession(id, token, response.updateTime)
                } else {
                    return@let null
                }
            }
        }

        mSession.allowNulls()

        mConfig = mSession.flatMap {
            if (it == null) {
                LinkedLiveData.NULL
            } else {
                val result: LinkedLiveData<Holder2<LukaConfig, LukaSession>> = mLukaServiceProvider.api()
                    .config()
                    .withAutoLaunch()
                    .withAutoRetry(6, TimeUnit.SECONDS)
                    .data
                    .combineWith(LiveDatas.from(it))
                result
            }
        }

        mConfig.allowNulls()
    }

    private fun config(): LiveData<Holder2<LukaConfig, LukaSession>> {
        return mConfig.then { config, next ->
            val isSessionInvalid = config?.value2?.hasExpired ?: true

            if (!isSessionInvalid) {
                next.value(config)
            }
        }
    }

    internal fun authenticate(credentials: LukaAuthCredentials) {
        mCredentials.user = credentials.user
        mCredentials.password = credentials.password
    }

    internal fun bluesnapAuth(): LiveData<BluesnapAuth> {
        return config().flatMap { _, session ->
            mLukaServiceProvider.api()
                .token()
                .withAutoLaunch()
                .response
                .map { response ->
                    response.headers()?.let {
                        val token = it["bstoken"]

                        if (token != null) {
                            return@let BluesnapAuth(session.id, token)
                        } else {
                            return@let null
                        }
                    }
                }
        }
    }

    @MainThread
    internal fun setup(caller: ActivityResultCaller) {
        mLukaResponse = MutableLiveData()
        val enumMap = EnumMap<LukaGateway, ActivityResultLauncher<LukaPaymentRequest>>(LukaGateway::class.java)

        mCallbacks = enumMap.apply {
            LukaGateway.values().forEach { gateway ->
                //Register activity results
                val registerForActivityResult = caller.registerForActivityResult(gateway.resolver()) {
                    mLukaResponse.value = it
                }

                put(gateway, registerForActivityResult)
            }
        }
    }

    fun clear() {
        mCallbacks = EnumMap(LukaGateway::class.java)
    }

    /**
     * Creates a new payment request
     */
    @MainThread
    @CheckResult
    fun createPaymentRequest(params: LukaPaymentParams): TransactionBuilder<out Any?, LukaTransactionProgress, LukaPaymentResult> {
        val requestData = config().flatMap { config, session ->
            LukaPaymentRequest(params, this, config, session)
                .callback()
        }.map { req: LukaPaymentRequest?, isSuccessful ->
            if (isSuccessful) {
                req
            } else {
                null
            }
        }.allowNulls()

        return PaymentTransactionBuilder(requestData)
    }

    @MainThread
    @CheckResult
    internal fun prepareTransaction(transaction: LukaTransaction): ApiTransactionBuilder<AuthorizedTransaction, LukaBridgeResponse> {
        val response = mSession.flatMap {
            mLukaServiceProvider.api()
                .authorizeTransaction(transaction)
                .withAutoLaunch()
                .response
        }

        return ApiTransactionBuilder(response)
    }

    @MainThread
    @CheckResult
    internal fun chargeTransaction(session: LukaSession, transaction: LukaTransaction): ApiTransactionBuilder<AuthorizedTransaction, LukaBridgeResponse> {
        val response = mLukaServiceProvider.api()
            .chargeTransaction(session.bearerToken, transaction)
            .withAutoLaunch()
            .response

        return ApiTransactionBuilder(response)
    }

    @MainThread
    @CheckResult
    fun checkTransaction(traceId: String): TransactionBuilder<out Any?, out Any?, LukaPaymentResult> {
        val response = mLukaServiceProvider.api()
            .checkTransaction(mCredentials.basic, traceId)
            .withAutoLaunch()
            .response

        return object : TransactionBuilder<ApiResponse<AuthorizedTransaction>, Any, LukaPaymentResult>(response) {
            override fun onExecute(input: ApiResponse<AuthorizedTransaction>) {
                when {
                    input.isSuccessful -> {
                        onSuccess?.invoke(LukaPaymentResult(input.data(), traceId))
                    }
                    input.hasError() -> {
                        try {
                            onError?.invoke(LukaHttpException(input.error(ApiErrorResponse::class)))
                        } catch (e: Exception) {
                            onError?.invoke(e)
                        }
                    }
                    input.hasFailed() -> {
                        onError?.invoke(input.exception()!!)
                    }
                }
            }
        }
    }
}