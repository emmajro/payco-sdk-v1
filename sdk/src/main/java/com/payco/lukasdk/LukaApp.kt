/*
 * Copyright (c) 2021. Soluciones Informáticas Manzanares - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited,
 * proprietary and confidential.
 * Written by Pedro Parra <pedrop@manzanares.com.ve>.
 * Last Modified 26/8/21 4:04 p. m.
 */

package com.payco.lukasdk

import com.payco.lukasdk.services.auth.LukaAuthCredentials

interface LukaApp {
    fun getLukaCredentials(): LukaAuthCredentials

    fun onLukaBuild(builder: LukaApiBuilder) {

    }
}