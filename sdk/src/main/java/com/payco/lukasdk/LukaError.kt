/*
 * Copyright (c) 2021. Soluciones Informáticas Manzanares - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited,
 * proprietary and confidential.
 * Written by Pedro Parra <pedrop@manzanares.com.ve>.
 * Last Modified 9/9/21 3:31 p. m.
 */

package com.payco.lukasdk

enum class LukaError(val defaultDescription: String) {
    SDK_FAILED("SDK Failed to process the request"),
    ABORTED_BY_USER("User aborted the checkout process"),
    TRANSACTION_REJECTED("Transaction was rejected by LukaPay"),
}