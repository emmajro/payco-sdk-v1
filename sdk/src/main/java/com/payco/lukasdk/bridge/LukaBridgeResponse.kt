/*
 * Copyright (c) 2021. Soluciones Informáticas Manzanares - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited,
 * proprietary and confidential.
 * Written by Pedro Parra <pedrop@manzanares.com.ve>.
 * Last Modified 9/9/21 12:12 p. m.
 */

package com.payco.lukasdk.bridge

internal data class LukaBridgeResponse(
    val action: LukaPaymentStep,
    val payload: Any
)