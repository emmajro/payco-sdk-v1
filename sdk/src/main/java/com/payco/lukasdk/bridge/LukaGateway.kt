/*
 * Copyright (c) 2021. Soluciones Informáticas Manzanares - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited,
 * proprietary and confidential.
 * Written by Pedro Parra <pedrop@manzanares.com.ve>.
 * Last Modified 9/9/21 9:37 a. m.
 */

package com.payco.lukasdk.bridge

import androidx.activity.result.contract.ActivityResultContract
import com.payco.lukasdk.bridge.bluesnap.BlueSnapBridge
import com.payco.lukasdk.bridge.bluesnap.BlueSnapResolver
import com.payco.lukasdk.transaction.LukaPaymentRequest

/**
 * Internal luka gateways
 */
internal enum class LukaGateway {
    Bluesnap {
        override fun bridge(): LukaBridge {
            return BlueSnapBridge()
        }

        override fun onMakeResolver(): BlueSnapResolver {
            return BlueSnapResolver()
        }
    };

    private lateinit var mResolver: ActivityResultContract<LukaPaymentRequest, LukaBridgeResponse>

    /**
     * Bridge instance to communicate
     */
    internal abstract fun bridge(): LukaBridge

    protected abstract fun onMakeResolver(): ActivityResultContract<LukaPaymentRequest, LukaBridgeResponse>

    /**
     * Resolver to register activity callbacks
     */
    internal fun resolver(): ActivityResultContract<LukaPaymentRequest, LukaBridgeResponse> {
        if (!::mResolver.isInitialized) {
            mResolver = onMakeResolver()
        }
        return mResolver
    }
}