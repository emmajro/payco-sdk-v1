/*
 * Copyright (c) 2021. Soluciones Informáticas Manzanares - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited,
 * proprietary and confidential.
 * Written by Pedro Parra <pedrop@manzanares.com.ve>.
 * Last Modified 8/9/21 2:08 p. m.
 */

package com.payco.lukasdk.bridge.bluesnap

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.bluesnap.androidapi.models.SdkRequest
import com.bluesnap.androidapi.models.SdkResult
import com.bluesnap.androidapi.services.BlueSnapService
import com.bluesnap.androidapi.services.BluesnapServiceCallback
import com.bluesnap.androidapi.services.TokenProvider
import com.bluesnap.androidapi.services.TokenServiceCallback
import com.payco.lukasdk.LukaApi
import com.payco.lukasdk.LukaCurrency
import com.payco.lukasdk.LukaError
import com.payco.lukasdk.bridge.LukaBridge
import com.payco.lukasdk.bridge.LukaBridgeResponse
import com.payco.lukasdk.bridge.LukaPaymentStep
import com.payco.lukasdk.exceptions.LukaHttpException
import com.payco.lukasdk.exceptions.PaymentFailedException
import com.payco.lukasdk.lifecycle.LiveDatas
import com.payco.lukasdk.lifecycle.once
import com.payco.lukasdk.services.luka.CardOwner
import com.payco.lukasdk.services.luka.LukaConfig
import com.payco.lukasdk.services.luka.payment.CreditCard
import com.payco.lukasdk.services.luka.transaction.LukaTransaction
import com.payco.lukasdk.transaction.LukaPaymentRequest
import com.payco.lukasdk.transaction.LukaPaymentResult

/**
 * BlueSnap bridge from Luka
 */
internal class BlueSnapBridge : LukaBridge {
    /**
     * LukaApi instance
     */
    private lateinit var mApi: LukaApi

    /**
     * Token manager
     */
    private lateinit var mTokenSwitcher: BluesnapTokenSwitcher

    /**
     * Result callback for setup
     */
    private lateinit var mSetupResult: MutableLiveData<Boolean>

    /**
     * Current valid token
     */
    private lateinit var mToken: String

    override fun setup(api: LukaApi, config: LukaConfig): LiveData<Boolean> {
        mApi = api
        mSetupResult = MutableLiveData()

        //Object to renew expired tokens
        mTokenSwitcher = BluesnapTokenSwitcher(api) {
            mToken = it
        }

        //Initialize SDK after first token retrieved
        mTokenSwitcher.mTokenHolder.once {
            BlueSnapService.getInstance()
                .setup(it, tokenProvider(), api.mContext, serviceCallback())
        }

        return mSetupResult
    }

    /**
     * BlueSnap token Provider. Provides a new token when the previous one expires
     */
    private fun tokenProvider(): TokenProvider {
        return object: TokenProvider {
            override fun getNewToken(tokenServiceCallback: TokenServiceCallback?) {
                //sends tue current token Back
                tokenServiceCallback?.complete(mToken)

                //triggers the token renewal procedure
                mTokenSwitcher.recycle()
            }
        }
    }

    /**
     * Setup response callback
     */
    private fun serviceCallback(): BluesnapServiceCallback {
        return object: BluesnapServiceCallback {
            override fun onSuccess() {
                mSetupResult.postValue(true)
            }

            override fun onFailure() {
                mSetupResult.postValue(false)
            }

        }
    }

    override fun launch(
        action: LukaPaymentStep,
        request: LukaPaymentRequest,
        payload: Any?
    ): LiveData<LukaBridgeResponse> {
        return when (action) {
            LukaPaymentStep.BEGIN -> begin(request)
            LukaPaymentStep.PREPARE -> prepare(request, payload as SdkResult)
            LukaPaymentStep.CHARGE -> charge(request, payload as LukaTransaction)
            else -> throw RuntimeException("Unknown action $action for bridge {$javaClass}")
        }
    }

    private fun begin(request: LukaPaymentRequest): LiveData<LukaBridgeResponse> {
        val params = request.params

        val sdkRequest = SdkRequest(
            params.amount,
            params.currency.iso,
            false,
            params.email == null,
            false
        )

        sdkRequest.isGooglePayTestMode = mApi.mTestMode
        sdkRequest.isActivate3DS = params.enable3dSecureAuthentication

        BlueSnapService.getInstance().sdkRequest = sdkRequest

        mApi.mCallbacks[request.gateway]!!.launch(request)

        return mApi.mLukaResponse
    }

    private fun prepare(request: LukaPaymentRequest, payload: SdkResult): LiveData<LukaBridgeResponse> {
        val transaction = LukaTransaction(
            cardOwnerEmail = request.params.email ?: payload.billingContactInfo.email,
            traceId = request.mSession.id,
            currency = LukaCurrency.valueOf(payload.currencyNameCode!!),
            amount = payload.amount,
            ref = "",
            creditCard = CreditCard(
                last4 = payload.last4Digits,
                expiresAt = payload.expDate,
                countryCode = payload.billingContactInfo.country,
                type = payload.cardType,
                category = "CONSUMER",
                subType = "CREDIT",
            ),
            creditCardOwner = CardOwner(
                name = payload.billingContactInfo.firstName,
                lastName = payload.billingContactInfo.lastName,
            ),
            bluesnapToken = payload.token
        )

        return LiveDatas.from(LukaBridgeResponse(LukaPaymentStep.CHARGE, transaction))
    }

    private fun charge(request: LukaPaymentRequest, transaction: LukaTransaction): LiveData<LukaBridgeResponse> {
        return mApi.chargeTransaction(request.mSession, transaction)
            .onSuccess {
                LukaBridgeResponse(
                    LukaPaymentStep.FINISH_SUCCESS,
                    LukaPaymentResult(request.params, transaction.traceId)
                )
            }
            .onError {
                if (it is LukaHttpException) {

                    //TODO map LukaPay responses

                    val message = "LukaPay responded with status = ${it.error.responseCode}"

                    LukaBridgeResponse(
                        LukaPaymentStep.FINISH_ERROR,
                        PaymentFailedException(LukaError.TRANSACTION_REJECTED, message)
                    )
                } else {
                    //failed to connect
                    LukaBridgeResponse(
                        LukaPaymentStep.FINISH_ERROR,
                        it
                    )
                }


            }
            .chain()
    }
}