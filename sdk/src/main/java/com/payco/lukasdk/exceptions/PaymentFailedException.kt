/*
 * Copyright (c) 2021. Soluciones Informáticas Manzanares - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited,
 * proprietary and confidential.
 * Written by Pedro Parra <pedrop@manzanares.com.ve>.
 * Last Modified 7/9/21 10:43 a. m.
 */

package com.payco.lukasdk.exceptions

import com.payco.lukasdk.LukaError

/**
 * Luka API error response
 */
class PaymentFailedException(val error: LukaError, description: String? = null) : Exception() {
    val description = description ?: error.defaultDescription

    override fun toString(): String {
        return "(error = $error, description = $description)"
    }
}