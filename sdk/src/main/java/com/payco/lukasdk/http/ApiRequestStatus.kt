/*
 * Copyright (c) 2020 Soluciones Informáticas Manzanares - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited,
 * proprietary and confidential.
 * Written by Pedro Parra <pedrop@manzanares.com.ve>.
 * Last Modified 23/11/20 04:14 PM.
 */
package com.payco.lukasdk.http

import android.os.SystemClock
import androidx.annotation.CheckResult
import java.util.concurrent.atomic.AtomicLong

/**
 * ApiRequest status handler
 */
internal class ApiRequestStatus {
    private val launchDate = AtomicLong(0L)
    private val responseDate = AtomicLong(0L)
    private val requiredDate = AtomicLong(0L)
    @CheckResult
    fun setOutdated(): Boolean {
        val launch = !isOutdated
        requiredDate.set(SystemClock.elapsedRealtime())
        return launch
    }

    val isOutdated: Boolean
        get() = responseDate.get() < requiredDate.get()

    fun setLaunched() {
        launchDate.set(SystemClock.elapsedRealtime())
    }

    fun setReplied() {
        responseDate.set(launchDate.get())
    }
}