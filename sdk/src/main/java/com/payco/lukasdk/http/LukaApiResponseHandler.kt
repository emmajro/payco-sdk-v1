/*
 * Copyright (c) 2019 Soluciones Informáticas Manzanares - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited,
 * proprietary and confidential.
 * Written by Pedro Parra <pedrop@manzanares.com.ve>.
 * Last Modified 02/07/19 05:44 PM
 */
package com.payco.lukasdk.http

import androidx.annotation.MainThread
import androidx.lifecycle.MutableLiveData
import com.payco.lukasdk.utils.Logger
import okhttp3.Headers
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.IOException
import java.util.concurrent.atomic.AtomicInteger

/**
 * Triggers response events to listeners and interceptors
 */
internal class LukaApiResponseHandler<T>(
    private val delegated: MutableLiveData<ApiResponse<T>>,
    private val interceptor: (T) -> T,
    private val listener: (T) -> Unit,
    private val httpListener: (Response<T>) -> Unit,
    private val headersListener: (Headers) -> Unit
) : Callback<T> {

    @Volatile
    private var mWrapper: LukaApiResponseWrapper<T>? = null
    private val attemptsCount = AtomicInteger(0)

    fun execute(call: Call<T>, async: Boolean) {
        if (async) {
            attemptsCount.set(0)
            synchronized(this) { delegated.postValue(newResponse().setLoading()) }
            call.enqueue(this)
        } else {
            try {
                val response = call.execute()
                onResponse(call, response)
            } catch (e: IOException) {
                onFailure(call, e)
            }
        }
    }

    override fun onResponse(call: Call<T>?, response: Response<T>) {
        onResponse(response)
        synchronized(this) {
            delegated.postValue(
                newResponse().setResponse(
                    response,
                    interceptor,
                    listener
                )
            )
        }
    }

    override fun onFailure(call: Call<T>, t: Throwable) {
        log.error(t, "Request Failed")
        if (attemptsCount.incrementAndGet() < MAX_ATTEMPTS) {
            call.clone().enqueue(this)
        } else {
            synchronized(this) { delegated.postValue(newResponse().setError(t)) }
        }
    }

    fun replaceData(newData: T) {
        synchronized(this) {
            val w = mWrapper
            if (w != null && w.isSuccessful) {
                delegated.postValue(w.setData(newData, interceptor, listener))
            }
        }
    }

    fun response(): ApiResponse<T>? {
        return mWrapper
    }

    private fun newResponse(): LukaApiResponseWrapper<T> {
        val out: LukaApiResponseWrapper<T> = mWrapper?.copy() ?: LukaApiResponseWrapper()
        this.mWrapper = out
        return out
    }

    private fun onResponse(response: Response<T>) {
        httpListener.invoke(response)
        headersListener.invoke(response.headers())
    }

    @MainThread
    fun finishLoad() {
        synchronized(this) {
            val wrapper = mWrapper
            if (wrapper != null && wrapper.isLoading()) {
                delegated.value = newResponse().setNotLoading()
            }
        }
    }

    companion object {
        private val log = Logger.getLogger(LukaApiResponseHandler::class)
        private const val MAX_ATTEMPTS = 1
    }
}