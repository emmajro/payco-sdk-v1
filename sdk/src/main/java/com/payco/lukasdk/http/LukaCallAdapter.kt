/*
 * Copyright (c) 2018 Soluciones Informáticas Manzanares - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited,
 * proprietary and confidential.
 * Written by Pedro Parra <pedrop@manzanares.com.ve>.
 * Last Modified 19/11/18 11:08 AM
 */
package com.payco.lukasdk.http

import retrofit2.Call
import retrofit2.CallAdapter
import java.lang.reflect.Constructor
import java.lang.reflect.Type

/**
 * CallAdapter wrapper
 */
internal class LukaCallAdapter<T>(
    private val responseType: Type,
    private val rawType: Class<*>
) : CallAdapter<T, ApiRequest<T>> {

    override fun responseType(): Type {
        return responseType
    }

    override fun adapt(call: Call<T>): ApiRequest<T> {
        return try {
            val rawType = rawType as Class<out ApiRequest<T>>
            val constructor: Constructor<*> = rawType.getDeclaredConstructor(
                Call::class.java
            )
            constructor.newInstance(call) as ApiRequest<T>
        } catch (e: Throwable) {
            throw RuntimeException("Unable to instantiate $rawType", e)
        }
    }
}