/*
 * Copyright (c) 2018 Soluciones Informáticas Manzanares - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited,
 * proprietary and confidential.
 * Written by Pedro Parra <pedrop@manzanares.com.ve>.
 * Last Modified 19/11/18 11:08 AM
 */
package com.payco.lukasdk.http

import com.payco.lukasdk.Luka
import okhttp3.Interceptor
import okhttp3.Response
import java.io.IOException

internal class LukaHeaderInterceptor : Interceptor {

    @Throws(IOException::class)
    override fun intercept(chain: Interceptor.Chain): Response {
        var request = chain.request()
        val builder = request.newBuilder()
        builder.addHeader("Accept", "application/json")

        if (request.header(AUTHORIZATION_GUEST) != null) {
            builder.removeHeader(AUTHORIZATION_GUEST)
        } else if (request.header(AUTHORIZATION_HEADER) == null) {
            val token: String? = try {
                Luka.api().mSession.value?.bearerToken
            } catch (ignored: Throwable) {
                null
            }

            if (token != null) {
                builder.addHeader(AUTHORIZATION_HEADER, token)
            }
        }

        request = builder.build()
        return chain.proceed(request)
    }

    companion object {
        internal const val NO_AUTH = "Guest: true"
        internal const val AUTHORIZATION_GUEST = "Guest"
        internal const val AUTHORIZATION_HEADER = "Authorization"
    }
}