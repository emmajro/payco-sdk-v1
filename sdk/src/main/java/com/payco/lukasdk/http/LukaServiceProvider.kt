package com.payco.lukasdk.http

import com.payco.lukasdk.services.auth.Auth
import com.payco.lukasdk.services.luka.Api
import retrofit2.Retrofit

/**
 * Luka Services
 */
internal class LukaServiceProvider(
    retrofit: Retrofit,
): HttpServiceProvider(retrofit) {
    fun auth(): Auth {
        return getService(Auth::class)
    }

    fun api(): Api {
        return getService(Api::class)
    }
}