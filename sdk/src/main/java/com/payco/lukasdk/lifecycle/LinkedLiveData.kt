/*
 * Copyright (c) 2021. Soluciones Informáticas Manzanares - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited,
 * proprietary and confidential.
 * Written by Pedro Parra <pedrop@manzanares.com.ve>.
 * Last Modified 27/8/21 1:03 p. m.
 */

package com.payco.lukasdk.lifecycle

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import com.payco.lukasdk.utils.Threads
import java.util.*

/**
 * LiveData that emits when another linked LiveData emits and applies a function for the content.
 */
open class LinkedLiveData<Type> : MediatorLiveData<Type>() {
    private var doOnNull: (() -> Type?) = {
        null
    }

    private var doOnError: ((Throwable) -> Type?) = {
        throw it
    }

    private var doAsync: Boolean = false
    private var avoidDuplicates: Boolean = true
    private var avoidNulls: Boolean = true

    private fun applyLinkedValue(emittedValue: Type?) {
        if (avoidNulls && emittedValue == null) {
            return
        }

        if (avoidDuplicates && emittedValue === value) {
            return
        }

        if (doAsync) {
            postValue(emittedValue)
        } else {
            value = emittedValue
        }
    }

    internal fun <SourceType> addSourceForOperation(
        source: LiveData<out SourceType?>,
        async: Boolean,
        function: (SourceType, Next<Type>) -> Unit
    ): LinkedLiveData<Type> {
        doAsync = async

        super.addSource(source) {
            if (it == null) {
                applyLinkedValue(doOnNull.invoke())
                return@addSource
            }

            val operation: () -> Unit = {
                try {
                    function.invoke(it, object : Next<Type> {
                        override fun value(emittedValue: Type) {
                            applyLinkedValue(emittedValue)
                        }

                        override fun value(): Type? {
                            return value
                        }
                    })
                } catch (t: Throwable) {
                    applyLinkedValue(doOnError.invoke(t))
                }
            }

            if (doAsync) {
                Threads.queueLatestAsync(function, operation)
            } else {
                operation.invoke()
            }
        }

        return this
    }

    internal fun addSource(source: LiveData<Type>) {
        super.addSource(source) {
            applyLinkedValue(it)
        }
    }

    internal fun <SourceType> addSourceForMap(
        source: LiveData<out SourceType?>,
        async: Boolean,
        function: (SourceType) -> Type
    ): LinkedLiveData<Type> {
        return addSourceForOperation(source, async) { value, next ->
            next.value(function.invoke(value))
        }
    }

    internal fun <SourceType> addSourceForSwitchMap(
        source: LiveData<SourceType>,
        function: (SourceType) -> LiveData<out Type>?
    ): LinkedLiveData<Type> {
        super.addSource(source, object : Observer<SourceType?> {
            var mSource: LiveData<out Type?>? = null

            override fun onChanged(input: SourceType?) {

                val newLiveData = (if (input == null) {
                    wrap(doOnNull.invoke())
                } else {
                    function.invoke(input)
                }) ?: NULL

                if (mSource === newLiveData) {
                    return
                }

                if (mSource != null) {
                    removeSource(mSource!!)
                }

                mSource = newLiveData
                addSource(mSource!!) { applyLinkedValue(it) }
            }
        })

        return this
    }

    fun onNull(func: () -> Type?): LinkedLiveData<Type> {
        doOnNull = func
        return this
    }

    fun onError(func: (Throwable) -> Type?): LinkedLiveData<Type> {
        doOnError = func
        return this
    }

    fun async(async: Boolean = true): LinkedLiveData<Type> {
        doAsync = async
        return this
    }

    fun allowDuplicates(allow: Boolean = true): LinkedLiveData<Type> {
        avoidDuplicates = allow
        return this
    }

    fun allowNulls(allow: Boolean = true): LinkedLiveData<Type> {
        avoidNulls = allow
        return this
    }

    internal companion object {
        val NULL: LinkedLiveData<Nothing?> = LiveDatas.from(null)
        val EMPTY = Object()

        internal fun <Input> wrap(value: Input?): LiveData<out Input?> {
            if (value == null) {
                return NULL
            }

            val result = MutableLiveData<Input?>()
            result.value = value
            return result
        }
    }
}