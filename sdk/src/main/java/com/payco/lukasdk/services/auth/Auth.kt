/*
 * Copyright (c) 2021. Soluciones Informáticas Manzanares - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited,
 * proprietary and confidential.
 * Written by Pedro Parra <pedrop@manzanares.com.ve>.
 * Last Modified 30/8/21 3:17 p. m.
 */

package com.payco.lukasdk.services.auth

import com.payco.lukasdk.http.ApiRequest
import com.payco.lukasdk.http.LukaHeaderInterceptor.Companion.NO_AUTH
import retrofit2.http.Body
import retrofit2.http.Headers
import retrofit2.http.POST

/**
 * Luka Auth services
 */
internal interface Auth {

    /**
     * Service to login to Luka. This responds with the authorization token
     * @see LukaAuthCredentials
     */
    @Headers(NO_AUTH)
    @POST("servicio/login")
    fun login(@Body body: LukaAuthCredentials): ApiRequest<Map<String, Any>>
}