/*
 * Copyright (c) 2021. Soluciones Informáticas Manzanares - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited,
 * proprietary and confidential.
 * Written by Pedro Parra <pedrop@manzanares.com.ve>.
 * Last Modified 30/8/21 3:18 p. m.
 */

package com.payco.lukasdk.services.auth

import android.os.SystemClock
import com.google.gson.annotations.SerializedName
import java.util.concurrent.TimeUnit

/**
 * Response of [Auth.login] endpoint.
 */
internal data class LukaSession(
    /**
     * The id of the user
     */
    @SerializedName("id")
    val id: String,

    /**
     * The token to authenticate Luka requests with.
     */
    @SerializedName("token")
    val token: String,

    /**
     * The token to authenticate Luka requests with.
     */
    val retrievedAt: Long,
) {
    val hasExpired: Boolean
        get() {
            return TimeUnit.MILLISECONDS.toMinutes(SystemClock.elapsedRealtime() - retrievedAt) > 10
        }

    val bearerToken = "Bearer $token"
}