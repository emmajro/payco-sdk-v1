/*
 * Copyright (c) 2021. Soluciones Informáticas Manzanares - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited,
 * proprietary and confidential.
 * Written by Pedro Parra <pedrop@manzanares.com.ve>.
 * Last Modified 3/9/21 12:59 p. m.
 */

package com.payco.lukasdk.services.luka


import com.google.gson.annotations.SerializedName
import com.payco.lukasdk.LukaCurrency
import com.payco.lukasdk.services.luka.bluesnap.BluesnapCredentials

/**
 * User config
 */
internal data class LukaConfig(
    @SerializedName("BluesnapApiCredentials")
    val bluesnapCredentials: BluesnapCredentials,

    @SerializedName("Color")
    val color: String,

    @SerializedName("Idioma")
    val lang: String,

    @SerializedName("MetodosPago")
    val paymentMethods: List<String>,

    @SerializedName("Moneda")
    val currency: LukaCurrency,

    @SerializedName("NumeroDecimales")
    val decimalsCount: Int,

    @SerializedName("SeparadorDecimal")
    val decimalsSeparator: String,

    @SerializedName("SeparadorMiles")
    val thousandsSeparator: String
)