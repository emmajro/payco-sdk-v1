/*
 * Copyright (c) 2021. Soluciones Informáticas Manzanares - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited,
 * proprietary and confidential.
 * Written by Pedro Parra <pedrop@manzanares.com.ve>.
 * Last Modified 6/9/21 12:56 p. m.
 */

package com.payco.lukasdk.services.luka.bluesnap


import com.google.gson.annotations.SerializedName

/**
 * Bluesnap credentials.
 */
internal data class BluesnapCredentials(
    @SerializedName("Password")
    val password: String,

    @SerializedName("RecaudaPayco")
    val usePaycoMiddleware: Boolean,

    @SerializedName("Username")
    val username: String
)