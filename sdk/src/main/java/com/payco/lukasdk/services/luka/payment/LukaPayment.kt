/*
 * Copyright (c) 2021. Soluciones Informáticas Manzanares - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited,
 * proprietary and confidential.
 * Written by Pedro Parra <pedrop@manzanares.com.ve>.
 * Last Modified 6/9/21 12:56 p. m.
 */

package com.payco.lukasdk.services.luka.payment


import com.google.gson.annotations.SerializedName
import com.payco.lukasdk.LukaCurrency
import com.payco.lukasdk.services.luka.CardOwner

/**
 * Luka payment model
 */
data class LukaPayment(
    @SerializedName("Canal")
    val channel: String,

    @SerializedName("Descripcion")
    val description: String,

    /**
     * Whether the payment was successful or not.
     */
    @SerializedName("Exitoso")
    val isSuccessful: Boolean,

    @SerializedName("InfoProceso")
    val processInfo: Any?,

    @SerializedName("InfoTarjeta")
    val creditCard: CreditCard,

    @SerializedName("InfoUsuarioPagador")
    val userInfo: Any?,

    @SerializedName("MedioDePago")
    val paymentMethod: String,

    @SerializedName("Moneda")
    val currency: LukaCurrency,

    @SerializedName("Monto")
    val amount: Double,

    @SerializedName("MontoOriginal")
    val originalAmount: Double?,

    @SerializedName("MontoUsd")
    val usdAmount: Double,

    @SerializedName("TarjetaHabiente")
    val cardOwner: CardOwner?,

    @SerializedName("TransaccionId")
    val transactionId: Long,

    @SerializedName("TransaccionMerchantId")
    val transactionMerchantId: Int,

    /**
     * Payment trace id
     */
    @SerializedName("TrazaId")
    val traceId: String
)