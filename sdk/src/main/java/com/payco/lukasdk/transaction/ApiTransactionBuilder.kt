/*
 * Copyright (c) 2021. Soluciones Informáticas Manzanares - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited,
 * proprietary and confidential.
 * Written by Pedro Parra <pedrop@manzanares.com.ve>.
 * Last Modified 9/9/21 9:35 a. m.
 */

package com.payco.lukasdk.transaction

import androidx.annotation.CheckResult
import androidx.lifecycle.LiveData
import com.payco.lukasdk.exceptions.LukaHttpException
import com.payco.lukasdk.http.ApiErrorResponse
import com.payco.lukasdk.http.ApiResponse
import com.payco.lukasdk.lifecycle.then

class ApiTransactionBuilder<Type, Output>(
    private val response: LiveData<ApiResponse<Type>>
) {
    protected var onSuccess: ((Type) -> Output)? = null
    protected var onError: ((Throwable) -> Output)? = null

    /**
     * Registers the transaction successful callback
     */
    @CheckResult
    fun onSuccess(action: (Type) -> Output): ApiTransactionBuilder<Type, Output> {
        onSuccess = action
        return this
    }

    /**
     * Registers the transaction failure callback
     */
    @CheckResult
    fun onError(action: (Throwable) -> Output): ApiTransactionBuilder<Type, Output> {
        onError = action
        return this
    }

    /**
     * Launches the transaction chain
     */
    @CheckResult
    fun chain(): LiveData<Output> {
        return response.then { apiResponse, next ->
            if (next.value() == null) {
                next.value(onExecute(apiResponse))
            }
        }
    }

    private fun onExecute(result: ApiResponse<Type>): Output? {
        return when {
            result.isSuccessful -> {
                 onSuccess?.invoke(result.data())
            }
            result.hasError() -> {
                try {
                    onError?.invoke(LukaHttpException(result.error(ApiErrorResponse::class)))
                } catch (e: Exception) {
                    onError?.invoke(e)
                }
            }
            result.hasFailed() -> {
                onError?.invoke(result.exception()!!)
            }
            else -> null
        }
    }
}