/*
 * Copyright (c) 2021. Soluciones Informáticas Manzanares - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited,
 * proprietary and confidential.
 * Written by Pedro Parra <pedrop@manzanares.com.ve>.
 * Last Modified 9/9/21 9:37 a. m.
 */

package com.payco.lukasdk.transaction

import com.payco.lukasdk.LukaCurrency
import com.payco.lukasdk.bridge.LukaMethod

/**
 * Parameters needed to charge with LukaPay
 */
data class LukaPaymentParams(
    /**
     * The choosen payment method
     */
    val method: LukaMethod,

    /**
     * The amount to charge
     */
    val amount: Double,

    /**
     * The currency for the charge
     */
    val currency: LukaCurrency,

    /**
     * The email is mandatory for LukaPay. If not specified, It will be asked in the payment form
     */
    val email: String? = null,

    /**
     * Enablee 3D Secure authentication
     */
    val enable3dSecureAuthentication: Boolean = false,
)