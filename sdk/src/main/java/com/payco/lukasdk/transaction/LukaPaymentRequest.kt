/*
 * Copyright (c) 2021. Soluciones Informáticas Manzanares - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited,
 * proprietary and confidential.
 * Written by Pedro Parra <pedrop@manzanares.com.ve>.
 * Last Modified 9/9/21 10:46 a. m.
 */

package com.payco.lukasdk.transaction

import com.payco.lukasdk.LukaApi
import com.payco.lukasdk.lifecycle.Holder2
import com.payco.lukasdk.lifecycle.LinkedLiveData
import com.payco.lukasdk.lifecycle.LiveDatas
import com.payco.lukasdk.lifecycle.combineWith
import com.payco.lukasdk.services.auth.LukaSession
import com.payco.lukasdk.services.luka.LukaConfig

internal class LukaPaymentRequest(
    val params: LukaPaymentParams,
    api: LukaApi,
    config: LukaConfig,
    val mSession: LukaSession,
) {
    val method = params.method
    val gateway = method.gateway()
    val bridge = gateway.bridge()
    val setupResult = bridge.setup(api, config)

    fun callback(): LinkedLiveData<Holder2<LukaPaymentRequest, Boolean>> {
        return LiveDatas.from(this)
            .combineWith(setupResult)
    }
}