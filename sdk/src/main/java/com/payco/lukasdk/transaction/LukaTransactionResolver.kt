/*
 * Copyright (c) 2021. Soluciones Informáticas Manzanares - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited,
 * proprietary and confidential.
 * Written by Pedro Parra <pedrop@manzanares.com.ve>.
 * Last Modified 9/9/21 3:00 p. m.
 */

package com.payco.lukasdk.transaction

import androidx.activity.result.ActivityResultCaller
import androidx.activity.result.contract.ActivityResultContract

internal abstract class LukaTransactionResolver<I, O>(
    private val caller: ActivityResultCaller
) : ActivityResultContract<I, O>() {

    fun resolve(callback : (O) -> Unit) {
        caller.registerForActivityResult(this, callback)
    }

}