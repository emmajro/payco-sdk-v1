/*
 * Copyright (c) 2021. Soluciones Informáticas Manzanares - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited,
 * proprietary and confidential.
 * Written by Pedro Parra <pedrop@manzanares.com.ve>.
 * Last Modified 3/9/21 3:33 p. m.
 */

package com.payco.lukasdk.utils

import java.util.*

/**
 * Log writer for testing
 */
internal class TestLogDispatcher: LogDispatcher() {
    private fun write(type: String, tag: String, message: String, t: Throwable? = null) {
        val date = Date().toString()
        println("$date $type/$tag: $message")

        if (t != null) {
            print(throwableMessage(t))
        }
    }

    override fun d(tag: String, message: String) {
        write("D", tag, message)
    }

    override fun d(t: Throwable, tag: String, message: String) {
        write("D", tag, message, t)
    }

    override fun i(tag: String, message: String) {
        write("I", tag, message)
    }

    override fun i(t: Throwable, tag: String, message: String) {
        write("I", tag, message, t)
    }

    override fun w(tag: String, message: String) {
        write("W", tag, message)
    }

    override fun w(t: Throwable, tag: String, message: String) {
        write("W", tag, message, t)
    }

    override fun e(tag: String, message: String) {
        write("E", tag, message)
    }

    override fun e(t: Throwable, tag: String, message: String) {
        write("E", tag, message, t)
    }
}